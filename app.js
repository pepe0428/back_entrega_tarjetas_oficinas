'use strict'

//Cargar modulos de node para crear servidor
var express=require('express');
var bodyParse=require('body-parser');//recibir peticiones en modo json

//JWT
//var jwt = require('jsonwebtoken');
//var fs = require('fs');

//Ejecutar express(http)
var app=express();

//Cargar ficheros rutas
var user_routes=require('./routes/user');
var oficina_routes=require('./routes/oficina');
var tarjeta_routes=require('./routes/tarjeta');
var solicitud_tarjeta_routes=require('./routes/solicitudTarjeta');

//Middlewars
app.use(bodyParse.urlencoded({extended:false}));
app.use(bodyParse.json());
//CORS


//Anadir prefijos a rutas /cargar rutas
app.use('/api',user_routes);
app.use('/api',oficina_routes);
app.use('/api',tarjeta_routes);
app.use('/api',solicitud_tarjeta_routes);


//Exportar modulo(fichero actual)
module.exports=app;
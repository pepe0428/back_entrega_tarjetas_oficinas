'use strict'
const config=require('../config/global.js');
var SolicitudTajeta=require('../models/solicitudTajeta');
var DetalleSolcitudTarjeta=require('../models/DetalleSolcitudTarjeta');

var controller = {

    save: (req, res) => {
        
        var params=req.body;
        var token=req.headers['authorization']
        console.log(token);
        let privateKey = config.fs.readFileSync('./config/private.pem', 'utf8');
        if(!token){
            res.status(401).send({
                error: 'Es necesario el token de autenticación'
              })
        }

        token=token.replace('Bearer ','')

        config.jwt.verify(token,privateKey,function(err,user){
            if(err){
                res.status(401).send({
                    error: 'Token inválido'
                  })
            }else{
                
                var solicitudTajeta=new SolicitudTajeta();
                solicitudTajeta.cdSolicitud=params.cdSolicitud;
                solicitudTajeta.cdOficina=params.cdOficina;
                solicitudTajeta.cantidad=params.cantidad;
                solicitudTajeta.cdTarjeta=params.cdTarjeta;
                solicitudTajeta.username=params.username;
                solicitudTajeta.fechaCreacion=params.fechaCreacion;
                solicitudTajeta.fechaMoficacion=params.fechaMoficacion;
                solicitudTajeta.fechaEntrega=params.fechaEntrega;
                solicitudTajeta.estado=params.estado;
                solicitudTajeta.cdMotivo=params.cdMotivo;
                solicitudTajeta.fechaAnulacion=params.fechaAnulacion;

                solicitudTajeta.save();

                var detalleSolcitudTarjeta=new DetalleSolcitudTarjeta();
                detalleSolcitudTarjeta.cdSolicitud=;
                detalleSolcitudTarjeta.cdTarjeta=;
                detalleSolcitudTarjeta.cantidad=;    


                return res.status(200).send({
                    status:'success',
                    message:'Se registro correctamente la Solicitud de Tarjetas'
                });

            }

        })
    }

}
module.exports=controller;
'use strict'
const config=require('../config/global.js');
var User=require('../models/user');

var controller = {
    //**Login**/
    login: (req, res) => {

      var params=req.body;

      User.findOne({username:params.username}, function(err,user) {
     
      if(err){
        return res.status(500).send({
            status:'error',
            error: 'usuario o contraseña inválidos'
          });
      }


      if(user === null || user === undefined){
        return res.status(500).send({
          status:'error',
          error: 'usuario o contraseña inválidos'
        });
        }

        /**Password**/
        const encrypted = config.crypto.createHmac('sha1', config.KEY)
        .update(params.password)
        .digest('base64')
    
      if( !(user.username === params.username &&  user.password === encrypted)){
        res.status(500).send({
          error: 'usuario o contraseña inválidos'
        })
        return

      }else{
        user.password='';  
        let privateKey = config.fs.readFileSync('./config/private.pem', 'utf8');
        let token = config.jwt.sign({ "body": "stuff" }, privateKey, { algorithm: 'HS256'});
    
        return res.status(200).send({
          status:'success',
          token: token,
          usuario:user,
          message:'token'
          });

      }
    
      });

      },

      save: (req, res) => {

        var params=req.body;
        var token=req.headers['authorization']
        console.log(token);
        let privateKey = config.fs.readFileSync('./config/private.pem', 'utf8');
        if(!token){
            res.status(401).send({
                error: 'Es necesario el token de autenticación'
              })
        }

        token=token.replace('Bearer ','')

        config.jwt.verify(token,privateKey,function(err,user){
            if(err){
                res.status(401).send({
                    error: 'Token inválido'
                  })
            }else{
                //config.secret
                const encrypted = config.crypto.createHmac('sha1', config.KEY)
                          .update(params.password)
                          .digest('base64')
        
                  var user=new User();
        
                  user.username=params.username;
                  user.password=encrypted;
                  user.email=params.email;
                  user.perfil=params.perfil;
                  user.nombres=params.nombres;
                  user.apellidos=params.apellidos;
                  user.estado=params.estado;
        
                  user.save();
        
                  return res.status(200).send({
                    status:'success',
                    message:'Se registro correctamente el usuario'
                    });

            }

        })
      }

}

module.exports=controller;
'use strict'
const config=require('../config/global.js');
var Tarjeta=require('../models/tarjeta');

var controller = {

    save: (req, res) => {

        var params=req.body;
        var token=req.headers['authorization']
        console.log(token);
        let privateKey = config.fs.readFileSync('./config/private.pem', 'utf8');
        if(!token){
            res.status(401).send({
                error: 'Es necesario el token de autenticación'
              })
        }

        token=token.replace('Bearer ','')

        config.jwt.verify(token,privateKey,function(err,user){
            if(err){
                res.status(401).send({
                    error: 'Token inválido'
                  })
            }else{
        
                  var tarjeta=new Tarjeta();
        
                  tarjeta.cdTarjeta=params.cdTarjeta;
                  tarjeta.nombre=params.nombre;
                  tarjeta.formato=params.formato;
                  tarjeta.imagenFrente=params.imagenFrente;
                  tarjeta.imagenReverso=params.imagenReverso;
                  tarjeta.rango=params.rango;
                  tarjeta.bin=params.bin;
                  tarjeta.codigo=params.codigo;
                  tarjeta.estado=params.estado;        
                  tarjeta.save();
        
                  return res.status(200).send({
                    status:'success',
                    message:'Se registro correctamente el tarjeta'
                    });

            }

        })
      },
      getAll: (req,res) => {

        var params=req.body;
        var token=req.headers['authorization']
        console.log(token);
        let privateKey = config.fs.readFileSync('./config/private.pem', 'utf8');
        if(!token){
            res.status(401).send({
                error: 'Es necesario el token de autenticación'
              })
        }

        token=token.replace('Bearer ','')

        config.jwt.verify(token,privateKey,function(err,user){
            if(err){
                res.status(401).send({
                    error: 'Token inválido'
                  })
            }else{

                  Tarjeta.find({estado:config.estadoActivo}, function(err,tarjetas) {
     
                    if(err){
                      return res.status(500).send({
                          status:'error',
                          error: 'usuario o contraseña inválidos'
                        });
                    }
                    console.log(tarjetas);
                    if(tarjetas==null){

                      res.status(500).send({
                        error: 'no se encontraron tarjetas'
                      })
                      return

                    }else{

                      return res.status(200).send({
                        status:'success',
                        data:tarjetas
                        });

                    }

                    

                    })
            }
        })
      }









    }

module.exports=controller;      
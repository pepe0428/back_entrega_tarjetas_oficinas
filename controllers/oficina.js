'use strict'
const config=require('../config/global.js');
var Oficina=require('../models/oficina');

var controller = {

    save: (req, res) => {
        
        var params=req.body;
        var token=req.headers['authorization']
        console.log(token);
        let privateKey = config.fs.readFileSync('./config/private.pem', 'utf8');
        if(!token){
            res.status(401).send({
                error: 'Es necesario el token de autenticación'
              })
        }

        token=token.replace('Bearer ','')

        config.jwt.verify(token,privateKey,function(err,user){
            if(err){
                res.status(401).send({
                    error: 'Token inválido'
                  })
            }else{
                
                var oficina=new Oficina();
                oficina.cdOficina=params.cdOficina;
                oficina.cdPadre=params.cdPadre;
                oficina.cdEmpresa=params.cdEmpresa;
                oficina.cdAlterno=params.cdAlterno;
                oficina.cdTerritorio=params.cdTerritorio;
                oficina.estado=params.estado;
                console.log(oficina);
                oficina.save();

                return res.status(200).send({
                    status:'success',
                    message:'Se registro correctamente la oficina'
                });

            }

        })
    }
}
module.exports=controller;
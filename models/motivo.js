'use strict'

var mongoose =require('mongoose');
var Schema=mongoose.Schema;


const MotivoSchema = new Schema({
    cdMotivo:  Number,
    nombre:    String,
    estado:    String

})

module.exports = mongoose.model('Motivo', MotivoSchema)
'use strict'

var mongoose =require('mongoose');
var Schema=mongoose.Schema;


const SolicitudTarjetaSchema = new Schema({
    cdSolicitud:    Number,
    cdOficina:      Number,
    cantidad:       Number,
    cdTarjeta:      Number,
    username:       String,
    fechaCreacion:  Date ,
    fechaMoficacion:Date,
    fechaEntrega :  Date,
    estado:         String,
    cdMotivo:       String,
    fechaAnulacion: Date


})

module.exports = mongoose.model('SolicitudTarjeta', SolicitudTarjetaSchema)
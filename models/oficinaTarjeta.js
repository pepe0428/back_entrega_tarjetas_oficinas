'use strict'

var mongoose =require('mongoose');
var Schema=mongoose.Schema;


const OficinaTarjetaSchema = new Schema({
    cdOficina:      Number,
    cdTarjeta:      Number,
    cantidad:       Number,
    cantidadMin:    Number,//cantidad minima que debe tener
    stVigente:      String,
    estado:         String

})

module.exports = mongoose.model('Oficina', OficinaTarjetaSchema)
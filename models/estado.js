'use strict'

var mongoose =require('mongoose');
var Schema=mongoose.Schema;


const EstadoSchema = new Schema({
    cdEstado:  Number,
    nombre:    String,
    estado:    String

})

module.exports = mongoose.model('Estado', EstadoSchema)
'use strict'

var mongoose =require('mongoose');
var Schema=mongoose.Schema;


const tarjetaSchema = new Schema({
    cdTarjeta:  Number,
    nombre:     String,
    formato:    String,
    imagenFrente:String,
    imagenReverso:String,
    rango:      String,
    bin:     String,
    codigo:     String,
    estado:     String

})

module.exports = mongoose.model('Tarjeta', tarjetaSchema)
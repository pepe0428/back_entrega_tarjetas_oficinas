'use strict'

var mongoose =require('mongoose');
var Schema=mongoose.Schema;


const DetalleSolicitudTarjetaSchema = new Schema({
    cdSolicitud:Number,
    cdTarjeta:  Number,
    cantidad: Number
})

module.exports = mongoose.model('DetalleSolicitudTarjetaSchema', DetalleSolicitudTarjetaSchema)
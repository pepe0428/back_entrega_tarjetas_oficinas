'use strict'

var mongoose =require('mongoose');
var Schema=mongoose.Schema;


const UserSchema = new Schema({
    username: String,
    password: String,
    perfil:   String,
    nombres:  String,
    apellidos:String,
    email:    String,
    estado:   String

})

module.exports = mongoose.model('User', UserSchema)

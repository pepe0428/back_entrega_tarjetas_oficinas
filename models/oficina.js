'use strict'

var mongoose =require('mongoose');
var Schema=mongoose.Schema;


const OficinaSchema = new Schema({
    cdOficina:   Number,
    cdPadre:     Number,
    cdEmpresa:   String,
    desAlterno:  String,
    cdTerritorio:String,
    estado:      String
})

module.exports = mongoose.model('Oficina', OficinaSchema)
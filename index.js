'use strict'
var mongoose=require('mongoose');
var app=require('./app');
var port= 3900;

mongoose.set('useFindAndModify',false);
mongoose.Promise=global.Promise;
const urlBd='mongodb://localhost:27017/bd_entrega_tarjeta';
mongoose.connect(urlBd,{useNewUrlParser:true})
.then(()=>{
    console.log('La conexion a la base de datos esta OK!');
    //Crear servidor y ponerme a escuchar peticiones HTTP
    app.listen(port,()=>{
        console.log('Servidor corriendo en http://localhost:3900')
;    })
})
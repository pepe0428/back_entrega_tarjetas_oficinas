
'use strict'

const estadoActivo='activo';
const estadoInactivo='inactivo';
const KEY='SeCrEtKeYfOrHaShInG';
const jwt = require('jsonwebtoken');
const fs = require('fs');
const crypto = require('crypto')
var validator=require('validator');


module.exports={
    KEY,jwt,fs,crypto,validator,estadoActivo,estadoInactivo
}
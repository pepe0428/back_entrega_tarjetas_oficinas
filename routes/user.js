'use strict'

var express= require('express');
var UserController=require('../controllers/user');

var router=express.Router();
router.post('/oauth', UserController.login);
router.post('/user/save', UserController.save);

module.exports=router;
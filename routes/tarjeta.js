'use strict'

var express= require('express');
var TarjetaController=require('../controllers/tarjeta');

var router=express.Router();
router.post('/tarjeta/save', TarjetaController.save);
router.get('/tarjeta/getAll', TarjetaController.getAll);

module.exports=router;
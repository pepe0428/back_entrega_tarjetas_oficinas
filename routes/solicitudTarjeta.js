'use strict'

var express= require('express');
var solicitudTarjetaController=require('../controllers/solicitudTarjeta');

var router=express.Router();
router.post('/solicitudTarjeta/save', solicitudTarjetaController.save);

module.exports=router;
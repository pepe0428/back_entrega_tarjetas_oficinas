'use strict'

var express= require('express');
var OficinaController=require('../controllers/oficina');

var router=express.Router();
router.post('/oficina/save', OficinaController.save);

module.exports=router;